using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly PaymentContext _context;
        public VendaController(PaymentContext context)
        {
            _context = context;
        }

        [HttpPost("CriandoPedidoDeVenda")]
        public IActionResult CriarPedido(Venda venda)
        {
            venda.Status = EStatusVenda.Aguardando_Pagamento;
            venda.Data = DateTime.Now;

            var vendedor = _context.Vendedors.Find(venda.VendedorId);
            if(vendedor == null)
                return BadRequest(new {Error = "Vendedor não cadastrado no sistema!"});

            var item = _context.Items.Find(venda.ItemId);
            if(item == null)
                return BadRequest(new {Error = "Item não cadastrado no sistema!"});

            _context.Add(venda);
            _context.SaveChanges();
            return CreatedAtAction(nameof(VisualizarPedidoPorId), new {id = venda.Id}, venda);
        }

        [HttpPost("AtualizarStatusDoPedido")]
        public IActionResult AtualizarStatusDaVenda(int id, Venda pedido)
        {
            var pedidoBanco = _context.Vendas.Find(id);
            if(pedidoBanco == null)
                return NotFound("Pedido não encontrado");

            pedidoBanco.Status = pedido.Status;

            _context.SaveChanges();
            return Ok(pedido);
        }

        [HttpGet("VisualizarPedidoPorId")]
        public IActionResult VisualizarPedidoPorId(int id)
        {
            var venda = _context.Vendas.Find(id);
            var vendedor = _context.Vendedors.Find(venda.VendedorId);
            var item = _context.Items.Find(venda.ItemId);
            if(venda == null)
                return NotFound("Esse pedido não existe.");
            
            return Ok(venda);
        }
    }
}
